# ISBI2020

Videos of 3PHS map of real collagen motion sequence for motion field computed with the proposed TV-regularized, CS data-term based variational method.

Videos are in tif format. We recommend using Fiji or ImageJ sofware to view the videos.

The right panel in the video shows the orthogonal projections of the volume data in xy, xz and zy planes.
The left panel depicts the corresponding 3PHS map of the computed flow field between current and next volume frames.